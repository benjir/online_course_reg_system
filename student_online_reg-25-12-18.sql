-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 25, 2018 at 10:13 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `student_online_reg`
--

-- --------------------------------------------------------

--
-- Table structure for table `batch`
--

CREATE TABLE `batch` (
  `id` int(11) NOT NULL,
  `batch_no` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `batch`
--

INSERT INTO `batch` (`id`, `batch_no`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9),
(10, 10),
(11, 11),
(12, 12),
(13, 13),
(14, 14),
(15, 15),
(16, 16);

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `code` varchar(50) NOT NULL,
  `credit` decimal(13,2) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'ACTIVE',
  `is_offered` varchar(50) NOT NULL,
  `dept_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `name`, `code`, `credit`, `status`, `is_offered`, `dept_id`) VALUES
(1, 'Computer Fundamental', 'CSE-1101', '3.00', 'ACTIVE', 'YES', 1),
(2, 'Data Structure', 'CSE-2101', '3.00', 'ACTIVE', 'YES', 1),
(3, 'Algorithm', 'CSE-3101', '3.00', 'ACTIVE', 'NO', 1),
(4, 'Computer Network', 'CSE-4101', '3.00', 'ACTIVE', 'NO', 1),
(5, 'VLSI', 'CSE-3201', '3.00', 'ACTIVE', 'YES', 1),
(6, 'DLD', 'CSE-2201', '2.00', 'ACTIVE', 'NO', 1),
(7, 'Mathemathics', 'MATH-1201', '2.00', 'ACTIVE', 'NO', 1),
(8, 'English (1)', 'ENG-1301', '2.00', 'ACTIVE', 'YES', 1),
(9, 'Parmacy 1', 'PHA-2120', '4.00', 'ACTIVE', 'NO', 5),
(10, 'LAW 1', 'LAW-1301', '2.00', 'ACTIVE', 'NO', 2);

-- --------------------------------------------------------

--
-- Table structure for table `course_registration`
--

CREATE TABLE `course_registration` (
  `id` int(11) NOT NULL,
  `u_id` varchar(50) NOT NULL,
  `course_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'PENDING' COMMENT '(status) PENDING,ACCEPTED',
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `course_registration`
--

INSERT INTO `course_registration` (`id`, `u_id`, `course_id`, `dept_id`, `type_id`, `status`, `reg_date`) VALUES
(5, '1101', 1, 1, 3, 'PENDING', '2018-12-25 08:32:36'),
(6, '1101', 2, 1, 2, 'PENDING', '2018-12-25 08:34:39');

-- --------------------------------------------------------

--
-- Table structure for table `course_reg_time`
--

CREATE TABLE `course_reg_time` (
  `id` int(11) NOT NULL,
  `notice` text NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `semester_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Table structure for table `dept`
--

CREATE TABLE `dept` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `dept`
--

INSERT INTO `dept` (`id`, `name`) VALUES
(1, 'CSE'),
(2, 'LAW'),
(3, 'ENGLISH'),
(4, 'BBA'),
(5, 'PHARMACY'),
(6, 'ARCHITECTURE');

-- --------------------------------------------------------

--
-- Table structure for table `pre_course`
--

CREATE TABLE `pre_course` (
  `id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `pre_course_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `pre_course`
--

INSERT INTO `pre_course` (`id`, `course_id`, `pre_course_id`) VALUES
(1, 2, 3),
(2, 5, 6);

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`id`, `name`) VALUES
(1, 'B.Sc'),
(2, 'M.Sc');

-- --------------------------------------------------------

--
-- Table structure for table `semester`
--

CREATE TABLE `semester` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `id` int(11) NOT NULL,
  `year` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`id`, `year`) VALUES
(1, '2016'),
(2, '2017'),
(3, '2018'),
(4, '2019');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `u_id` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `current_address` varchar(200) NOT NULL,
  `permanent_address` varchar(200) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `prog_id` int(11) NOT NULL,
  `session_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `semester` varchar(50) NOT NULL,
  `activity_log` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `guardian_name` varchar(50) NOT NULL,
  `guardian_rel` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `u_id`, `name`, `email`, `phone`, `current_address`, `permanent_address`, `dept_id`, `prog_id`, `session_id`, `batch_id`, `semester`, `activity_log`, `guardian_name`, `guardian_rel`) VALUES
(4, '1003', 'Benjir Ahmed', 'benjirahmed89@gmail.com', '1671720037', 'Katasur 1', 'Dhaka', 1, 1, 3, 11, 'spring', '2018-12-24 00:27:21', 'father', 'Father'),
(5, '1101', 'test', 'test@mail.com', '1671720037', 'Katasur 1', 'Dhak', 1, 1, 4, 10, 'spring', '2018-12-25 06:28:06', 'John', 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

CREATE TABLE `teacher` (
  `id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(50) NOT NULL,
  `dept_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `teacher`
--

INSERT INTO `teacher` (`id`, `u_id`, `name`, `email`, `phone`, `dept_id`) VALUES
(2, 1002, 'Benjir Ahmed', 'benjirahmed@gmail.com', '1234567890', 1),
(3, 1004, 'Benjir Ahmed', 'benjirahd89@gmail.com', '1671720037', 1);

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE `type` (
  `id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`id`, `type`) VALUES
(1, 'G'),
(2, 'I'),
(3, 'R');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `u_id` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `user_role` varchar(50) NOT NULL DEFAULT 'STUDENT',
  `status` varchar(50) NOT NULL DEFAULT 'ACTIVE'
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `u_id`, `password`, `user_role`, `status`) VALUES
(1, '1001', '123', 'ADMIN', 'ACTIVE'),
(11, '1003', '123', 'STUDENT', 'ACTIVE'),
(12, '1004', '123', 'TEACHER', 'ACTIVE'),
(13, '1101', '123', 'STUDENT', 'ACTIVE');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `batch`
--
ALTER TABLE `batch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_dept3_id` (`dept_id`);

--
-- Indexes for table `course_registration`
--
ALTER TABLE `course_registration`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_course_id` (`course_id`),
  ADD KEY `fk_u_id` (`u_id`),
  ADD KEY `fk_dept5_id` (`dept_id`),
  ADD KEY `fk_type_id` (`type_id`);

--
-- Indexes for table `course_reg_time`
--
ALTER TABLE `course_reg_time`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_dept4_id` (`dept_id`),
  ADD KEY `fk_semester_id` (`semester_id`);

--
-- Indexes for table `dept`
--
ALTER TABLE `dept`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pre_course`
--
ALTER TABLE `pre_course`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_course6_id` (`course_id`),
  ADD KEY `fk_course7_id` (`pre_course_id`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `semester`
--
ALTER TABLE `semester`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `u_id` (`u_id`) USING BTREE,
  ADD KEY `fk_dept_id` (`dept_id`),
  ADD KEY `fk_prog_id` (`prog_id`),
  ADD KEY `fk_session_id` (`session_id`),
  ADD KEY `fk_semester_id` (`semester`),
  ADD KEY `fk_batch_id` (`batch_id`);

--
-- Indexes for table `teacher`
--
ALTER TABLE `teacher`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_dept2_id` (`dept_id`);

--
-- Indexes for table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `u_id` (`u_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `batch`
--
ALTER TABLE `batch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `course_registration`
--
ALTER TABLE `course_registration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `course_reg_time`
--
ALTER TABLE `course_reg_time`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dept`
--
ALTER TABLE `dept`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pre_course`
--
ALTER TABLE `pre_course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `program`
--
ALTER TABLE `program`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `semester`
--
ALTER TABLE `semester`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `session`
--
ALTER TABLE `session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `teacher`
--
ALTER TABLE `teacher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `type`
--
ALTER TABLE `type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `course`
--
ALTER TABLE `course`
  ADD CONSTRAINT `fk_dept3_id` FOREIGN KEY (`dept_id`) REFERENCES `dept` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `course_registration`
--
ALTER TABLE `course_registration`
  ADD CONSTRAINT `fk_course_id` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_dept5_id` FOREIGN KEY (`dept_id`) REFERENCES `dept` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_type_id` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_u_id` FOREIGN KEY (`u_id`) REFERENCES `user` (`u_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `course_reg_time`
--
ALTER TABLE `course_reg_time`
  ADD CONSTRAINT `fk_dept4_id` FOREIGN KEY (`dept_id`) REFERENCES `dept` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_semester_id` FOREIGN KEY (`semester_id`) REFERENCES `semester` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pre_course`
--
ALTER TABLE `pre_course`
  ADD CONSTRAINT `fk_course6_id` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_course7_id` FOREIGN KEY (`pre_course_id`) REFERENCES `course` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `student`
--
ALTER TABLE `student`
  ADD CONSTRAINT `fk_batch_id` FOREIGN KEY (`batch_id`) REFERENCES `batch` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_dept_id` FOREIGN KEY (`dept_id`) REFERENCES `dept` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_prog_id` FOREIGN KEY (`prog_id`) REFERENCES `program` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_session_id` FOREIGN KEY (`session_id`) REFERENCES `session` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `teacher`
--
ALTER TABLE `teacher`
  ADD CONSTRAINT `fk_dept2_id` FOREIGN KEY (`dept_id`) REFERENCES `dept` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
