<?php
//session_start();
include_once 'session.php';
include_once 'database.php';
include_once 't_header.php';
if (!isLoggedIn()) {
    header("Location: ../index.php");
}
$db = db_connect();
$teacher_id = $_SESSION['u_id'];
if ($db) {
  $teacher_dept_sql = "SELECT dept_id from teacher where u_id = '$teacher_id' ";
  $teacher_dept_query = mysqli_query($db,$teacher_dept_sql);
  if (mysqli_num_rows($teacher_dept_query) > 0 ) {
     while ($teacher_dept_result = mysqli_fetch_assoc($teacher_dept_query)){
       $teacher_dept_id = $teacher_dept_result['dept_id'];
       // var_dump($offered_dept_id);
       // exit();
     }
   }
 }
$sn = 1;
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3 class="text-center p-3 mb-2 mt-2 bg-secondary text-white">Offered Course List</h3>
            <!--  All Course Table Start -->
              <div class="table-responsive-md">
                  <table class="table table-bordered">
                    <thead class="thead-light">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Course Name</th>
                          <th scope="col">Course Code</th>
                          <th scope="col">Course Credit</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        if ($db) {
                          $course_sql = "SELECT * from course WHERE is_offered = 'YES' AND dept_id ='$teacher_dept_id'";
                          $course_query = mysqli_query($db,$course_sql);
                        }
                           if (mysqli_num_rows($course_query) > 0 ) {
                              while ($course_result = mysqli_fetch_assoc($course_query)){
                              // echo '<pre>';
                              // echo '</pre>';
                              // print_r($course_result);
                        ?>
                        <tr>
                          <th scope="row"><?php echo $sn ++;?></th>
                          <td><?php echo $course_result['name'];?></td>
                          <td><?php echo $course_result['code'];?></td>
                          <td><?php echo $course_result['credit'];?></td>
                        </tr>
                        <?php } } ?>
                      </tbody>
                  </table>
              </div>
              <!--  All Course Table Ends -->

        </div>
    </div>
</div>
<?php include_once 'footer.php';?>
