<?php
session_start();
include_once 'database.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Pre Registration</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="assets/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/font-awesome/font-awesome.min.css" integrity="sha256-k2/8zcNbxVIh5mnQ52A0r3a6jAgMGxFJFE2707UxGCk= sha512-ZV9KawG2Legkwp3nAlxLIVFudTauWuBpC10uEafMHYL0Sarrz5A7G79kXh5+5+woxQ5HM559XX2UZjMJ36Wplg==" crossorigin="anonymous">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
                                    <!--Header Start---------->
<header class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="logo"><a href="index.php"><img src="assets/img/logo.png"></a></div>
                <div class="headline"><h3>STATE UNIVERSITY OF BANGLADESH</h3></div>
            </div>
            <div class="col-md-4">
                <div class="phone">
                    <i class="fa fa-phone"></i> +900 6584 4858
                </div>
            </div>
        </div>
    </div>
</header>
                             <!--Header End------------>

                             <!--Login Form Start------------>
  <div class="container">
    <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
             <div class="user_login">
                   <?php
                         if(isset($_SESSION["login_error"])){
                             $login_error = $_SESSION["login_error"];
                             echo $login_error;
                         }
                   ?>
                   <form action="login.php" method="POST">
                       <div class="form-group">
                           <label for="InputUserID">User ID</label>
                           <input type="text" name="u_id" class="form-control" placeholder="Enter User ID" required>
                       </div>
                       <div class="form-group">
                           <label for="InputPassword">Password</label>
                           <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password" required>
                       </div>
                       <div class="form-group">
                             <input type="submit" name="submit" class="btn btn-primary btn-block" value="Login">
                       </div>
                    </form>
                 <!-- <div class="">
                    <p> Not Registered ? <a href="student_reg.php">Register Here</a></p>
                 </div> -->
             </div>
        </div>
        <div class="col-md-4">
        </div>

</div>
</div>
<?php include_once 'footer.php';?>

<?php unset($_SESSION["login_error"]);?>
                             <!--Login Form End------------>
