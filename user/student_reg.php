<?php
include_once 'database.php';
include_once 'session.php';
include_once 'header.php';
$db = db_connect();
?>
                        <!--Pre-Registration Form Start------------>
<div class="container">
    <div class="row">
        <div class="col-md-12">
          <?php
          if (isset($_GET['sm']) && $_GET['sm'] == "success") {
            echo "<div class='alert alert-success mt-2'><strong>STUDENT ACCOUNT REGISTRATION SUCCESSFUL! </strong></div>";
          }
          ?>
            <section>
                <div class="row">
                    <div class="col-md-12">
                      <h3 class="text-center p-3 mb-2 mt-2 bg-secondary text-white">Student Account Registration Form</h3></div>
                    <div class="col-md-6">
                  <form action="student_reg_p.php" method="post">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Student ID</label>
                            <div class="col-sm-8">
                              <input type="text" name="u_id" class="form-control" id="" placeholder="Student ID" required >
                            </div>
                        </div>
                        <!-- <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Password</label>
                            <div class="col-sm-8">
                              <input type="password" name="password" class="form-control" id="" placeholder="Your Login password" required >
                            </div>
                        </div> -->
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Student Name</label>
                            <div class="col-sm-8">
                                <input type="text" name="student_name" class="form-control" id="" placeholder="Student Name" required >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Guardian Name</label>
                            <div class="col-sm-8">
                                <input type="text" name="g_name" class="form-control" id="" placeholder="Guardian Name" required >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Relationship with the Guardian</label>
                            <div class="col-sm-8">
                                <div class="form-check">
                                    <input class="form-check-input" name="rel_with_g" type="radio" value="Father" id="defaultCheck1" required>
                                    <label class="form-check-label" for="defaultCheck1">Father</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" name="rel_with_g" type="radio" value="Mother" id="defaultCheck1" required >
                                    <label class="form-check-label" for="defaultCheck1">Mother</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" name="rel_with_g" type="radio" value="Other" id="defaultCheck1" required >
                                    <label class="form-check-label" for="defaultCheck1">Other</label>
                                </div>
                            </div>
                        </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Name of Department</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <select name="dept" class="form-control" id="exampleFormControlSelect1" required >
                                            <option value="">Select Department</option>
                                            <?php
                                            if ($db) {
                                                $dept_sql = "SELECT * FROM `dept` ";
                                                $dept_query = mysqli_query($db,$dept_sql);
                                              }
                                            if (mysqli_num_rows($dept_query) > 0){
                                                while ( $dept_result = mysqli_fetch_assoc($dept_query)){
                                            ?>
                                                <option value="<?php echo $dept_result['id']; ?>"> <?php echo $dept_result['name'];?> </option>
                                            <?php
                                                } }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Batch No</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <select name="batch" class="form-control" id="exampleFormControlSelect1" required >
                                            <option>Select Batch</option>
                                            <?php
                                            if ($db) {
                                                $batch_sql = "SELECT * FROM batch ";
                                                $batch_query = mysqli_query($db,$batch_sql);
                                            }
                                            if (mysqli_num_rows($batch_query) > 0) {
                                                while ( $batch_result = mysqli_fetch_assoc($batch_query)){
                                                ?>
                                                <option value="<?php echo $batch_result['id']; ?>"> <?php echo $batch_result['batch_no'];?></option>
                                            <?php
                                                } }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Program</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <select name="program" class="form-control" id="exampleFormControlSelect1" required >
                                            <option>Select Program</option>
                                            <?php
                                            if ($db) {
                                                $program_sql = "SELECT * FROM `program` ";
                                                $program_query = mysqli_query($db,$program_sql);
                                            }
                                            if (mysqli_num_rows($program_query) > 0) {
                                                while ( $program_result = mysqli_fetch_assoc($program_query)){
                                                ?>
                                                <option value="<?php echo $program_result['id']; ?>"> <?php echo $program_result['name'];?></option>
                                            <?php
                                                } }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Session</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <select name="session" class="form-control" id="exampleFormControlSelect1" required >
                                            <option>Select Session</option>
                                            <?php
                                            if ($db) {
                                                $session_sql = "SELECT * FROM `session` ";
                                                $session_query = mysqli_query($db,$session_sql);
                                            }
                                            if (mysqli_num_rows($session_query) > 0) {
                                                while ( $session_result = mysqli_fetch_assoc($session_query)){
                                                ?>
                                                <option value="<?php echo $session_result['id']; ?>"> <?php echo $session_result['year'];?></option>
                                            <?php
                                                } }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Semester</label>
                          <div class="col-sm-8">
                              <div class="form-check">
                                  <input class="form-check-input" name="semester" type="radio" value="spring" id="defaultCheck1" required >
                                  <label class="form-check-label" for="defaultCheck1">Spring</label>
                              </div>
                              <div class="form-check">
                                  <input class="form-check-input" name="semester" type="radio" value="summer" id="defaultCheck1" required >
                                  <label class="form-check-label" for="defaultCheck1">Summer</label>
                              </div>
                              <div class="form-check">
                                  <input class="form-check-input" name="semester" type="radio" value="fall" id="defaultCheck1" required >
                                  <label class="form-check-label" for="defaultCheck1">Fall</label>
                              </div>
                          </div>
                      </div>
                      <div class="form-group row">
                          <label class="col-sm-4 col-form-label">E:mail</label>
                          <div class="col-sm-8">
                              <input type="email" name="email" class="form-control" id="inputEmail3" placeholder="example@gmail.com" required >
                          </div>
                      </div>
                      <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Contact Number</label>
                          <div class="col-sm-8">
                              <input type="text" name="student_phone" class="form-control" id="inputEmail3" placeholder="01711123456" required >
                          </div>
                      </div>
                      <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Guardian Contact Number</label>
                          <div class="col-sm-8">
                              <input type="text" name="g_phone" class="form-control" id="inputEmail3" placeholder="01711123456">
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="inputAddress">Mailing Address</label>
                          <input type="text" name="current_address" class="form-control" id="inputAddress" placeholder="Mailing Address" required>
                      </div>
                      <div class="form-group">
                          <label for="inputAddress2">Permanent Address</label>
                          <input type="text" name="permanent_address" class="form-control" id="inputAddress2" placeholder="Permanent Address">
                      </div>
                      <!-- <div class="form-group">
                          <input type="text" name="status" value="INACTIVE" hidden>
                      </div> -->
                  </div>
                  </div>
                  <div class="container">
                      <div class="row">
                        <div class="col-md-5">
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                               <input type="submit" name="submit" class="btn btn-outline-primary btn-lg btn-block" value="Register">
                          </div>
                        </div>
                        <div class="col-md-4">
                        </div>
                      </div>
                    </form>
                  </div>
            </section>
        </div>
    </div>
  </div>
                              <!------------Pre-Registration FormEnd------------>
<?php include_once 'footer.php'; ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
