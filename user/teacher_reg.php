<?php
//session_start();
include_once 'session.php';
include_once 'database.php';
include_once 'header.php';
if (!isLoggedIn()) {
    header("Location: index.php");
}
$db = db_connect();

?>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <?php
      if (isset($_GET['sm']) && $_GET['sm'] == "success") {
        echo "<div class='alert alert-success mt-2'><strong>TEACHER ACCOUNT REGISTRATION SUCCESSFUL!</strong></div>";
      }
      ?>
      <h3 class="text-center p-3 mb-5 mt-2 bg-secondary text-white">Teacher Account Registration Form</h3>
    </div>
    </div>
    <div class="row">
    <div class="col-md-3">
    </div>
    <div class="col-md-6">
        <form class="" method="post" action="teacher_reg_p.php">
          <div class="form-group">
            <label for="Name">Name</label>
            <input type="text" name="name" class="form-control" placeholder="Enter Teacher Name">
          </div>
          <div class="form-group">
            <label for="Password">Password</label>
            <input type="password" name="password" class="form-control" placeholder="Enter Password">
          </div>
          <div class="form-group">
            <label for="UserID">UID</label>
            <input type="text" name="u_id" class="form-control" placeholder="Enter User ID">
          </div>
          <div class="form-group">
              <label class="col-form-label">Name of Department</label>
                  <div class="form-group">
                      <select name="dept" class="form-control" id="exampleFormControlSelect1" required >
                          <option value="">Select Department</option>
                          <?php
                          if ($db) {
                              $dept_sql = "SELECT * FROM dept ";
                              $dept_query = mysqli_query($db,$dept_sql);
                            }
                          if (mysqli_num_rows($dept_query) > 0){
                              while ( $dept_result = mysqli_fetch_assoc($dept_query)){
                          ?>
                              <option value="<?php echo $dept_result['id']; ?>"> <?php echo $dept_result['name'];?> </option>
                          <?php
                              } }
                          ?>
                      </select>
                  </div>
          </div>
          <div class="form-group">
            <label for="email">Email</label>
            <input type="email" name="email" class="form-control" placeholder="Enter Email">
          </div>
          <div class="form-group">
            <label for="Phone">Phone</label>
            <input type="text" name="phone" class="form-control" placeholder="Enter Phone No">
          </div>
          <button type="submit" name="submit" class="btn btn-outline-primary">Register</button>
        </form>
    </div>
    <div class="col-md-3">
    </div>
  </div>
</div>

<?php include_once 'footer.php';?>
