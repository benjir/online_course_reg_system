<?php
//session_start();
include_once 'session.php';
include_once 'database.php';
include_once 's_header.php';
if (!isLoggedIn()) {
    header("Location: index.php");
}
$db = db_connect();
$student_id = $_SESSION['u_id'];
//var_dump($user_id);
if ($db) {
  $student_dept_sql = "SELECT dept_id from student where u_id = '$student_id' ";
  $student_dept_query = mysqli_query($db,$student_dept_sql);
  //var_dump($user_id);
  if (mysqli_num_rows($student_dept_query) > 0 ) {
     while ($student_dept_result = mysqli_fetch_assoc($student_dept_query)){
       $student_dept_id = $student_dept_result['dept_id'];
     }}
$sn = 1;
?>
<div class="container">
  <div class="row">
    <div class="col-md-12">

    </div>

  </div>

</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3 class="text-center p-3 mb-2 mt-2 bg-secondary text-white">Offered Course List</h3>
            <?php
            if (isset($_GET['sm']) && $_GET['sm'] == "success") {
              echo "<div class='alert alert-success'><strong>Course Registration SUCCESSFUL !! Wait for the ACTIVATION !!</strong></div>";
            }
            ?>
            <!--  Offered Course Table Start -->
              <div class="table-responsive-md">
                  <table class="table table-bordered">
                    <thead class="thead-light">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Course Name</th>
                          <th scope="col">Course Code</th>
                          <th scope="col">Course Credit</th>
                          <th scope="col">Course Type</th>
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        if ($db) {
                          $course_sql = "SELECT * from course where dept_id = '$student_dept_id' ";
                          $course_query = mysqli_query($db,$course_sql);
                        }
                           if (mysqli_num_rows($course_query) > 0 ) {
                              while ($course_result = mysqli_fetch_assoc($course_query)){
                                $course_id = $course_result['id'];
                        ?>
                        <tr>
                          <th scope="row"><?php echo $sn ++;?></th>
                          <td><?php echo $course_result['name'];?></td>
                          <td><?php echo $course_result['code'];?></td>
                          <td><?php echo $course_result['credit'];?></td>
                          <td>
                            <div class="col-sm-8">
                            <form class="" action="offered_course_p.php" method="post">
                                <div class="form-group">
                                    <select name="type" class="form-control" id="exampleFormControlSelect1" required >
                                        <!-- <option>Select Type</option> -->
                                        <?php
                                        if ($db) {
                                            $type_sql = "SELECT * FROM type ";
                                            $type_query = mysqli_query($db,$type_sql);
                                        }
                                        if (mysqli_num_rows($type_query) > 0) {
                                            while ( $type_result = mysqli_fetch_assoc($type_query)){
                                            echo $type_result['type'];
                                        ?>
                                    </select>
                                </div>
                            </div>
                          </td>
                          <td>
                              <div class="form-group">
                                  <input type="hidden" name="course_id" value="<?php echo $course_id; ?>">
                                  <input type="hidden" name="dept_id" value="<?php echo $student_dept_id; ?>">
                                  <input type="submit" name="register" class="btn btn-outline-primary" value="Add">
                              </div>
                            </form>
                          </td>
                        </tr>
                      <?php } } } ?>
                      </tbody>
                  </table>
              </div>
              <!--  Offered Course Table Ends -->
        </div>
    </div>
</div>
<?php include_once 'footer.php';?>
