<?php
//session_start();
include_once 'session.php';
include_once 'database.php';
include_once 's_header.php';
if (!isLoggedIn()) {
    header("Location: index.php");
}
$db = db_connect();

?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3 class="text-center p-3 mb-2 mt-2 bg-secondary text-white">Current Semester Course List</h3>
            <!--  Current Semester Course Table Start -->
              <div class="table-responsive-md">
                  <table class="table table-bordered">
                    <thead class="thead-light">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Course Name</th>
                          <th scope="col">Course Code</th>
                          <th scope="col">Credit</th>
                          <!-- <th scope="col">Course Type</th> -->
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row"><?php echo "1";?></th>
                          <td><?php echo "DLD";?></td>
                          <td><?php echo "CSE-301";?></td>
                          <td><?php echo "3.00";?></td>
                          <!-- <td><?php //echo "G";?></td> -->
                        </tr>
                      </tbody>
                  </table>
              </div>
              <!--  Current Semester Course Table Ends -->
            <h3 class="text-center p-3 mb-2 bg-secondary text-white">Registered Course List</h3>
            <!--  Registered Course Table Start -->
              <div class="table-responsive-md">
                  <table class="table table-bordered">
                    <thead class="thead-light">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Course Name</th>
                          <th scope="col">Course Code</th>
                          <th scope="col">Credit</th>
                          <th scope="col">Course Type</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row"><?php echo "1";?></th>
                          <td><?php echo "DLD";?></td>
                          <td><?php echo "CSE-301";?></td>
                          <td><?php echo "3.00";?></td>
                          <td><?php echo "G";?></td>
                        </tr>
                      </tbody>
                  </table>
              </div>
              <!--  Registered Course Table Ends -->
        </div>
    </div>
</div>
<?php include_once 'footer.php';?>
